use firestore::errors::FirestoreError;
use rocket;

#[derive(Debug)]
pub enum UMErrors {
    RocketError(rocket::Error),
    FirestoreError(FirestoreError),
}

 impl From<rocket::Error> for UMErrors{
    fn from(value: rocket::Error) -> Self {
        Self::RocketError(value)
    }
}

impl From<FirestoreError> for UMErrors{
    fn from(value: FirestoreError) -> Self {
        Self::FirestoreError(value)
    }
}