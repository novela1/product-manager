use serde::{Deserialize, Serialize};
use crate::models::Model;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Project {
    pub name: String,


    #[serde(default="Option::default")]
    pub git_url: Option<String>,
}

impl Model for Project {
    const COLLECTION_NAME: &'static str = "projects";

    fn id(&self) -> String { self.name.to_string() }
}

impl Project {
    pub fn new(name:String) -> Project {Project{name, ..Default::default()}}

    pub fn set_git_url(&mut self, git_url:String) -> &mut Self { self.git_url = Some(git_url);self }
}