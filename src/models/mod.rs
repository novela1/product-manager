pub mod project;

use std::collections::HashMap;
use firestore::*;
use once_cell::sync::OnceCell;
use serde::{Deserialize, Serialize};
use firestore::errors::FirestoreError as FError;

pub use project::*;

use crate::errors::UMErrors;

static DB_INSTANCE: OnceCell<FirestoreDb> = OnceCell::new();


#[async_trait]
pub trait Model: Serialize + Sync + Send + Sized + for<'a> Deserialize<'a> {
    const COLLECTION_NAME: &'static str;
    fn id(&self) -> String;
    async fn save(&self) -> Result<(), FError> {
        db().fluent()
            .update()
            .in_col(Self::COLLECTION_NAME)
            .document_id(self.id().as_str())
            .object(self)
            .execute()
            .await?;
        Ok(())
    }

    async fn get<S, V>(conditions: HashMap<S, V>) -> Result<Vec<Self>, FError>
        where S: AsRef<str> + Send + Sync,
              V: Into<FirestoreValue> + Serialize + Send + Sync,
    {
        let result: Vec<Self> =
            db().fluent()
                .select()
                .from(Self::COLLECTION_NAME)
                .filter(|q| {
                    let mut conditions_expression = vec!();
                    for (k, v) in conditions.iter() {
                        conditions_expression.push(q.field(k).eq(v))
                    }
                    q.for_all(conditions_expression)
                })
                .obj()
                .query()
                .await?;
        Ok(result)
    }

    async fn get_one<S, V>(conditions: HashMap<S, V>) -> Result<Option<Self>, FError>
        where S: AsRef<str> + Send + Sync,
              V: Into<FirestoreValue> + Serialize + Send + Sync,
    {
        let mut get_result = Self::get(conditions).await?;
        Ok( if get_result.len() == 1 {Some(get_result.remove(0))} else {None})
    }

}

pub async fn init_db() -> Result<(), UMErrors> {
    DB_INSTANCE.set(
        FirestoreDb::new(&std::env::var("GCP_PROJECT_ID").unwrap())
            .await.expect("Can not init DB")
    ).expect("Can not init DB");
    Ok(())
}

#[inline]
pub fn db() -> &'static FirestoreDb {
    DB_INSTANCE.get().expect("Config not inited")
}