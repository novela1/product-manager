mod project_api;

use nrn_generator::PermissionAction;
pub use project_api::*;

use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};
use rocket::Request;

pub struct AuthToken(String);

#[derive(Debug)]
pub enum ApiTokenError {
    Missing,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthToken {
    type Error = ApiTokenError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.headers().get_one("Authorization") {
            Some(key) => Outcome::Success(AuthToken(key.to_string())),
            _ => Outcome::Failure((Status::Unauthorized, ApiTokenError::Missing)),
        }
    }
}

pub async fn check_access(target_nrn: String, action: PermissionAction) -> Result<(), Status> {
    Ok(())
}
