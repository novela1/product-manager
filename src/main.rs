mod models;
mod errors;
mod controllers;

#[macro_use] extern crate rocket;

use crate::errors::UMErrors;

#[get("/")]
async fn index() -> &'static str {
    "ok"
}

#[rocket::main]
async fn main() -> Result<(), UMErrors>  {

    models::init_db().await?;

    let _rocket = rocket::build()
        .mount("/", routes![index])
         .mount("/api/v1", routes![
                     controllers::create_product,
                 ])
        .launch()
        .await?;

    Ok(())
}
