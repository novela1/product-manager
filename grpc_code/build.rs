fn main () -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .out_dir("./src")
        .emit_rerun_if_changed(true)
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(&["api.proto"], &["./../protos"])?;
    
    Ok(())
}