mod models;
mod errors;
mod server;

use crate::errors::PMErrors;
use tokio;

#[tokio::main]
async fn main() -> Result<(), PMErrors>  {

    models::init_db().await?;


    Ok(())
}
