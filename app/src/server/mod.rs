use tonic::{
    metadata::{Ascii, MetadataMap, MetadataValue},
    Request, Response, Status,
};

use nl_pm_grpc::{
    product_management_server::ProductManagement, CreateNewRunRequest, CreateNewRunResponse, CreateProductRequest,
    Null, RunEvent,
};

pub struct PMService {}

#[tonic::async_trait]
impl ProductManagement for PMService {
    async fn create_new_product(&self, req: Request<CreateProductRequest>) -> Result<Response<Null>, Status> {
        todo!()
    }

    async fn create_new_product_run(
        &self,
        req: Request<CreateNewRunRequest>,
    ) -> Result<Response<CreateNewRunResponse>, Status> {
        todo!()
    }

    async fn push_product_run_event(&self, req: Request<RunEvent>) -> Result<Response<Null>, Status> {
        todo!()
    }
}
