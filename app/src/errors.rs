use firestore::errors::FirestoreError;

#[derive(Debug)]
pub enum PMErrors {
    FirestoreError(FirestoreError),
}


impl From<FirestoreError> for PMErrors{
    fn from(value: FirestoreError) -> Self {
        Self::FirestoreError(value)
    }
}