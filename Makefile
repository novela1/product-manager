# Load .env
ifneq ("$(wildcard .env)","")
    include .env
    export $(shell sed 's/=.*//' .env)
else
endif
.ONESHELL:
.EXPORT_ALL_VARIABLES:

WORKDIR := $(shell pwd)


help: ## Display help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'
	
run: ## Run the app
	cargo run -p nl-product-manager

run_with_watch: ## Run server and reload for each file changes
	cargo  watch -w app -c -x 'check -p nl-product-manager' -x 'build -p nl-product-manager' -s 'touch .trigger' &
	cargo watch  --no-vcs-ignores -w .trigger -x 'run -p nl-product-manager'


build_grpc_api: ## Build GRPC API
	cargo build  -p nl_pm_grpc